selector = (value) => document.querySelector(value); // for document.querySelector
id = (id) => document.getElementById(id); // For document.querySelector

let scores, roundScore, activePlayer, dice, gamePlaying;

init();


var lastDice;

// Roll Button
selector('.btn-roll').addEventListener('click', function () {

    if (gamePlaying) {
        // 1. Random number
        let dice1 = Math.floor(Math.random() * 6) + 1;
        let dice2 = Math.floor(Math.random() * 6) + 1;
        // console.log(dice);
        //2. Display the result

        // let diceDOM = selector('.dice');
        id('dice-1').style.display = 'block';
        id('dice-2').style.display = 'block';
        id('dice-1').src = 'img/dice-' + dice1 + '.png';
        id('dice-2').src = 'img/dice-' + dice2 + '.png';

        //3. Update the round score IF the rolled number was NOT a 1
        if (dice1 !== 1 && dice2 !== 1) {
            //Add Score
            roundScore += dice1 + dice2; // roundScore = roundScore + dice;
            selector('#current-' + activePlayer).textContent = roundScore;

        } else {
            nextPlayer();
        }
        
    }

});

selector('.btn-hold').addEventListener('click', function () {
    if (gamePlaying) {
        // Add current score to GLOBAL score 
        scores[activePlayer] += roundScore;

        // Update the UI
        selector('#score-' + activePlayer).textContent = scores[activePlayer];

        var input = selector('.final-score').value;
        // console.log(input);
        let winningScore;

        if (input) {
            winningScore = input;
        } else {
            winningScore = 100;
        }
        //Check if player won the game
        if (scores[activePlayer] >= winningScore) {
            id('name-' + activePlayer).textContent = 'Winner!'
            id('dice-1').style.display = 'none';
            id('dice-2').style.display = 'none';
            selector('.player-' + activePlayer + '-panel').classList.add('winner');
            selector('.player-' + activePlayer + '-panel').classList.remove('active');
            gamePlaying = false;

        } else {
            //Next player
            nextPlayer();
        }

    }

});

function nextPlayer() {
    //Next player
    activePlayer === 0 ? activePlayer = 1 : activePlayer = 0;
    roundScore = 0;
    id('current-0').textContent = '0';
    id('current-1').textContent = '0';

    //Change next player background to active
    selector('.player-0-panel').classList.toggle('active');
    selector('.player-1-panel').classList.toggle('active')

    // selector('.dice').style.display = 'none';
    id('dice-1').style.display = 'none';
    id('dice-2').style.display = 'none';
};

selector('.btn-new').addEventListener('click', init);

function init() {
    scores = [0, 0];
    roundScore = 0;
    activePlayer = 0;
    gamePlaying = true;

    id('dice-1').style.display = 'none';
    id('dice-2').style.display = 'none';

    id('score-0').textContent = '0';
    id('score-1').textContent = '0';
    id('current-0').textContent = '0';
    id('current-1').textContent = '0';
    id('name-0').textContent = 'Player 1';
    id('name-1').textContent = 'Player 2';
    selector('.player-0-panel').classList.remove('winner');
    selector('.player-1-panel').classList.remove('winner');
    selector('.player-0-panel').classList.remove('active');
    selector('.player-1-panel').classList.remove('active');
    selector('.player-0-panel').classList.add('active');
};